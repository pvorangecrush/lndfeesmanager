package strategies

import "github.com/lightningnetwork/lnd/lnrpc"

type Ignore struct{}

func (i *Ignore) Apply(*lnrpc.Channel, *lnrpc.RoutingPolicy, *lnrpc.RoutingPolicy) *Policy {
	return nil
}
