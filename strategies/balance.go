package strategies

import "github.com/lightningnetwork/lnd/lnrpc"
import "math"

type Balance struct {
	LocalUnbalancedLimit  int64
	RemoteUnbalancedLimit int64

	LocalUnbalancedFee  Policy
	RemoteUnbalancedFee Policy
	BalancedFee         Policy
}

func localbalancePercentage(c *lnrpc.Channel) int64 {
	return c.LocalBalance * 100 / (c.Capacity - c.LocalChanReserveSat - c.RemoteChanReserveSat)
}

func localMaxHtlc(c *lnrpc.Channel) int64 {
	return int64(math.Floor(float64(c.LocalBalance)/100) * 100000)
}

func (b *Balance) Apply(c *lnrpc.Channel, local, _ *lnrpc.RoutingPolicy) *Policy {
	localPercentage := localbalancePercentage(c)
	maxHtlc := localMaxHtlc(c)
	var policy Policy
	var slope float64
	if localPercentage > b.LocalUnbalancedLimit {
		policy = b.LocalUnbalancedFee
		var numerator float64 
		var denominator float64
		numerator = float64 (localPercentage - b.LocalUnbalancedLimit)
		denominator = float64 (100 - b.LocalUnbalancedLimit)
		slope = numerator / denominator
		policy.FeeRate = math.Round((b.BalancedFee.FeeRate + (slope * (b.LocalUnbalancedFee.FeeRate - b.BalancedFee.FeeRate)))*100000)/100000
		policy.MaxHtlcMsat = maxHtlc
	} else if localPercentage < b.RemoteUnbalancedLimit {
		policy = b.RemoteUnbalancedFee
		var numerator float64
		var denominator float64
		numerator = float64 (b.RemoteUnbalancedLimit - localPercentage)
		denominator = float64 (b.RemoteUnbalancedLimit)
		slope = numerator / denominator
		policy.FeeRate = math.Round((b.BalancedFee.FeeRate - (slope * (b.BalancedFee.FeeRate - b.RemoteUnbalancedFee.FeeRate)))*100000)/100000
		policy.MaxHtlcMsat = maxHtlc
	} else {
		policy = b.BalancedFee
		policy.MaxHtlcMsat = maxHtlc
	}

	changed := local.FeeBaseMsat != policy.BaseFeeMsat ||
		local.FeeRateMilliMsat != int64(policy.FeeRate*1000000) ||
		local.TimeLockDelta != policy.TimeLockDelta ||
		int64(local.MaxHtlcMsat) != policy.MaxHtlcMsat

	if !changed {
		return nil
	}

	return &policy
}
