package main

import (
	"flag"
	"os"
	"path"

	"gitlab.com/pvorangecrush/lndfeesmanager/strategies"

	"github.com/apex/log"
	"github.com/apex/log/handlers/cli"
	"gitlab.com/pvorangecrush/lndfeesmanager/fees"
)

var (
	address  string
	network  string
	reckless bool
	debug    bool
	daemon   bool
	dataDir  string
)

const (
	defaultAddress                   = "localhost:10009"
	defaultNetwork                   = "mainnet"
	defaultTimeLockDelta             = 40
	defaultLocalUnbalancedFeesLimit  = 70
	defaultRemoteUnbalancedFeesLimit = 30
)

func init() {
	ucd, err := os.UserConfigDir()
	if err != nil {
		log.WithError(err).Fatal("cannot get user config dir")
	}

	defaultDataDir := path.Join(ucd, "lndfeesmanager")

	flag.StringVar(&dataDir, "datadir", defaultDataDir, "the configuration folder")
	flag.StringVar(&address, "address", defaultAddress, "the LND host:port")
	flag.StringVar(&network, "network", defaultNetwork, "the bitcoin network (mainnet, testnet)")
	flag.BoolVar(&reckless, "reckless", false, "Update the fees automatically")
	flag.BoolVar(&debug, "v", false, "verbose logging")
	flag.BoolVar(&daemon, "d", false, "run as daemon")
}

func main() {
	flag.Parse()

	configFile := path.Join(dataDir, "config.toml")
	conf, err := LoadConfig(configFile)
	if err != nil {
		log.WithError(err).WithField("file", configFile).Fatal("cannot load configuration file")
	}
	overrideConf(conf)

	log.SetHandler(cli.Default)
	if conf.Verbose {
		log.SetLevel(log.DebugLevel)
	} else {
		log.SetLevel(log.InfoLevel)
	}

	strategiesFactory := &strategies.Factory{
		DefaultStrategy: getBalanceStrategy(conf.BalanceStrategy),
	}

	ignoreStrategy := &strategies.Ignore{}
	for _, chanid := range conf.IgnoreStrategy.Channels {
		strategiesFactory.Add(chanid, ignoreStrategy)
	}

	manager, err := fees.NewManager(
		conf.Lnd.Address,
		string(conf.Lnd.Network),
		strategiesFactory,
	)
	if err != nil {
		log.WithError(err).Fatal("Cannot start the manager")
	}
	defer manager.Close()

	if err := manager.CheckLndConnection(); err != nil {
		log.WithError(err).Fatal("Cannot connect to lnd")
	}

	if err := manager.Run(reckless, conf.Daemon, conf.Interval); err != nil {
		log.WithError(err).Fatal("Failed")
	}

	log.Warn("Bye!")
}

func overrideConf(conf *Config) {
	if address != defaultAddress {
		conf.Lnd.Address = address
	}

	if network != defaultNetwork {
		conf.Lnd.Network = Network(network)
	}

	if debug {
		conf.Verbose = true
	}

	if daemon {
		conf.Daemon = true
	}
}
